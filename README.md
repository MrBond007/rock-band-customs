# ROCK BAND CUSTOM SONG REPOSITORY

Since the closure of the Rock Band Network, and then the closure of the Wii Shop, it is impossible to get more songs from the Music Stores of Rock Band 2, 3, or The Beatles on the Wii. Thus, the files included in this repo will allow you to add new songs to those titles via an unconventional hacking method. A little programming knowledge is required, but this will be covered in the wiki.

# WARNING!

Please read the [Terms and Conditions](https://gitlab.com/HarvHouHacker/rock-band-customs/wikis/ZZZ-More-Information/Terms-and-Conditions) in my GitLab Wiki before attempting to use this software.

Any modifications may brick your Wii or Wii U. This means that it may make your Wii or Wii U unusable. I strongly recommend backing up saves and WiiWare to an SD, and for those who may be a bit ignorant (it's okay! We all were once), the NAND as well. If you do not want to mod, stop reading!

# What's Included?

For PC and Homebrew Channel software specifically for Rock Band, download the entire `master` branch, either by using `git`, or by downloading a compressed package (ZIP, TAR, or whatever) directly from here on GitLab.

## For Windows

*Windows 7 recommended, although this software can be used on versions as low as XP 64-bit. Since this software is directly from the source, right now they are only Windows-compatible. For Linux users, WINE **may** work. Anyone who tests this is welcome to report their findings, and create new issues when they arise.*

* **Xorloser's ArkTools:** Tools which extract and modify archives on game discs. **Do not share extracted archives!** Please reread any piracy warnings before using.
* **Album Make:** This software will make album covers for you, in `.png_wii` format. 
* **MahoppianGoon's Rock Band DLC Tools:**  Made mainly for Rock Band customs on the Xbox, it also has some very useful tools for the Wii versions. The tools include:
    - *OGG2MOGG:* This will convert multitrack OGG audio files into Rock Band compatible files with the MOGG extension.
    - *Songs.dta Editor:* This will aid in editing DTA files so that they can be read by Rock Band titles.
    - The other tools are mainly for Xbox users, but you are welcome to mess aroud with 'em anyway!'


## For Wii/vWii

* **xyzzy:** A small program which dumps console keys. To use, inject the entire folder into your SD Card's `apps` folder.

# DLC Packers

These are in two branches - `DLC_Packers_NTSC` and `DLC_Packers_PAL`. If using Git type `git checkout DLC_Packers_REGION` to switch to the ones you want. If downloading via ZIP or other archive, browse and download the right branch and extract to a project folder.

The WADs in `Ticket-WADs` are small updates which would have allowed you to download content from the Rock Band Music Store for free. Now, however, they're being used to add customs.

# Old Rock Band Harmonies Project Files

If using Git, simply checkout one of the branches below, else download the one(s) you want. Place the files in folders explained in the wiki and pack. *To prevent piracy of songs, files from official Rock Band discs and DLC have not been uploaded to the repo. You will have to extract them from discs or DLC that you yourself have purchased.*

* **DLC from Rock Band:** Look for them in `DLC_from_RB1`.
* **DLC from Rock Band 2:** Look for them in branch `DLC_from_RB2`.
* **DLC from LEGO Rock Band:** Look for them in branch `DLC_from_LRB`.
* **Rock Band Network DLC:** These are split into multiple branches. They are `RBN_DLC_2007`, `RBN_DLC_2008`, `RBN_DLC_2009`, `RBN_DLC_2010`, and `RBN_DLC_UGC`.
* **Rock Band Pro Droms:** Look for them in `RBN_Pro_Drums`^.
* **Optional Upgrades:** Look for them in `RB3_Upgrades`^.
 
*A ^ indicates that the branch has not been added yet.*

These are based off of the latest package from the source, however the files may be very old. For a more complete, up-to-date list, you may want to try the [Rock Band Harmonies Project on Github](https://github.com/rbhp-espher/rbhp).

# Links to Additional Software

These are links to other software that will be very useful for making Rock Band custom DLC.

* **C3 CON Tools:** Very useful for converting Xbox Rock Band files to Wii, as well as several analysers and file modifiers. [Download from Custom Creators.](http://customscreators.com/index.php?/topic/9095-c3-con-tools-v400-012518/)
* **WiiXplorer:** Much more user-friendly than FS Toolbox. It's a full-blown graphical file manager for the Wii, which allows cut-and-paste, and includes a few built-in file editors. [Download from Sourceforge.](https://sourceforge.net/projects/wiixplorer/)
* **ShowMiiWads:** A WAD manager for Windows. It can create the `common-key` file that you need for packing DLC. [Download from Sourceforge.](https://sourceforge.net/projects/showmiiwads/)
* **Some YAWMM (Yet Another WAD Manager Mod) Mod:** A WAD manager for the Homebrew Channel. It can install/uninstall WADs on your Wii. [Download from GitHub.](https://github.com/FIX94/Some-YAWMM-Mod)

# Special Thanks

* **StackOverflow0x/Koetsu** - This guy is the one who inspired me to go all-out on hacking Rock Band 3. You can check out his profiles on [Customs Creators (as StackOverflow0x](http://customscreators.com/index.php?/user/6620-stackoverflow0x/) and [ScoreHero (as Koetsu)](https://rockband.scorehero.com/scores.php?user=484319&diff=). My guide is based off of his guides, but his are a little different because they also explain how to use Xbox 360 files more in-depth than mine does.
* **Alternity** - He helped me understand how to use copyright material responsibly, and his suggestions on improving RBC Discord made it all the more manageable.
* **Farottone** - Also helped with understanding about copyrighted content, and helped out a great deal on RBC Discord.
* **Atruejedi** - He broke down the guide by StackO, explaining more about the whys as well as the the what-not-to-dos. His profile is on [Customs Creators](http://customscreators.com/index.php?/user/13921-atruejedi/).
* **MahoppianGoon** - His software and guides are mainly for Xbox 360, but his OGG2MOGG and songs.dta Editor Tools help a lot for Wii customs! Visit his site on [Google Sites](https://sites.google.com/site/mahoppiangoon/home).
* **Xorloser** - He created the Arktools suite which can extract ARK packages found in Rock Band discs, as well as convert songs from Gutar Hero to be used in Rock Band, among other things which I have yet to figure out. Find his blog on [WordPress](http://www.xorloser.com).